from django.urls import path
from receipts.views import (
    receipts_all,
    receipt_create,
    category_list,
    accounts_list,
    category_create,
    account_create,
)


urlpatterns = [
    path("", receipts_all, name="home"),
    path("create/", receipt_create, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", accounts_list, name="accounts_list"),
    path("categories/create/", category_create, name="create_category"),
    path("accounts/create/", account_create, name="create_account"),
]
