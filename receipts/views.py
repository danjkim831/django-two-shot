from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import (
    CreateReceiptForm,
    CreateExpenseCategoryForm,
    CreateAccountForm,
)


# Create your views here.


# views that gets ALL instances of the Receipt model:
# protect the list view for the Receipt model so only a person who is logged in can access it:
@login_required
def receipts_all(request):
    # why do i have to place place purchaser before request.user?  ANSWER: .filter(property=value)  look at references
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)


# view to CREATE a new receipt:
@login_required
def receipt_create(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)  # False or commit=False works!
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    elif request.method == "GET":
        form = CreateReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


# views for Expense Category List for the CURRENT USER:
@login_required
def category_list(request):
    """
    When filtering objects based on the current user, you should use
    the field that points to the user, which in this case is 'owner' in both
    'ExpenseCategory' and 'Account'
    """
    user_categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"user_categories": user_categories}
    return render(request, "receipts/categories.html", context)


# views for Account List for the CURRENT USER:
@login_required
def accounts_list(request):
    """
    Like above, the ForeignKey that links to the user is called 'owner', not
    'name'. The 'owner' field is the one that references the user who created
    the instance of 'Account'
    """
    user_accounts = Account.objects.filter(owner=request.user)
    context = {"user_accounts": user_accounts}
    return render(request, "receipts/accounts.html", context)


# views to CREATE an ExpenseCategory:
@login_required
def category_create(request):
    if request.method == "POST":
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)  # False or commit=False works!
            category.owner = request.user
            form.save()
            return redirect("category_list")

    elif request.method == "GET":
        form = CreateExpenseCategoryForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_category.html", context)


# views to CREATE an Account (Bank, Payment Method):
@login_required
def account_create(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)  # False or commit=False works!
            account.owner = request.user
            form.save()
            return redirect("accounts_list")

    elif request.method == "GET":
        form = CreateAccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_account.html", context)
