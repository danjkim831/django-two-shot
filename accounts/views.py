from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignUpForm


# Create your views here.


# views that logs a person in and then redirects to list page if POST / list.html if GET:
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            # verify a set of crendentials
            user = authenticate(
                request,
                username=username,
                password=password,
            )

            # authenticate checks for user or None:
            if user is not None:
                # log the user in, then redirects to the path named "home"
                login(request, user)
                return redirect("home")

    elif request.method == "GET":
        form = LoginForm()

    context = {
        "form": form,
    }

    return render(request, "accounts/login.html", context)


# views that logs a person out and then redirects them to the URL path named "login":
def user_logout(request):
    logout(request)
    return redirect("login")


# views to handle showing the SIGNUP form and handle its submission:
def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
            else:  # if password does NOT match password_confirmation
                form.add_error("password", "the paswords do not match")

            login(request, user)
            return redirect("home")

    elif request.method == "GET":
        form = SignUpForm()

    context = {
        "form": form,
    }

    return render(request, "accounts/signup.html", context)
